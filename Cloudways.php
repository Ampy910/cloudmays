<?php
/**
 * Plugin Name: Cloudways
 * Description: Create a woocommerce custom shipping method plugin
 */
if ( ! defined( 'WPINC' ) ){
    die('security by preventing any direct access to your plugin file');
}
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    function cloudways_shipping_method(){
        class wc_cloudways_Shipping_Method extends WC_Shipping_Method{
            public function __construct($instance_id = 0){
                $this->instance_id        = absint( $instance_id );
                $this->id = "wc_cloudways";
                $this->title = __('Cloudways Shipping', 'cloudways');
                $this->method_title = __('Cloudways Shipping', 'cloudways');
                $this->method_description = __('Custom Shipping Method for cloudways', 'cloudways');
                // Contreis availability
                $this->availability = 'including';
                $this->countries = array(
                    'CO');
                $this->supports = array(
                    'settings',
                    'shipping-zones',
                    'instance-settings',
                );
                $this->init_form_fields();
                $this->init_settings();
                add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
            }

            function init_form_fields(){
                $this->form_fields = array(
                    'weight' => array(
                        'title' => __('Weight (kg)', 'cloudways'),
                        'type' => 'number',
                        'default' => 70),

                    'title' => array(
                        'title' => __('Title', 'cloudways'),
                        'type' => 'text',
                        'default' => __('cloudways Shipping', 'cloudways')),
                    
                    'pago_minimo'=> array(
                        'title' => __('Pago Minimo', 'cloudways'),
                        'type' => 'number',
                        'default' => 100),

                    'precio_de_envio'=> array(
                        'title' => __('Presio de Envio', 'cloudways'),
                        'type' => 'number',
                        'default' => 100),
                    
                );
            }

            public function calculate_shipping($package = []){
                update_option("aaaa",json_encode($package));
                $precioDeEnvio = floatval($precioDeEnvio); 

                $precioDeEnvio = $this->get_option('precio_de_envio');
                $precioDeEnvio = floatval($precioDeEnvio);
                if($package["cart_subtotal"] > $precioDeEnvio);      
                
                $precioMinimo = $this->get_option("pago_minimo");
                $precioMinimo = floatval($precioMinimo);
                if($package["cart_subtotal"] > $precioMinimo){

                    $rate = array(
                        'id' => $this->id,
                        'label' => "Envio Gratis",
                        'cost' => 0);
                    $this->add_rate($rate);
                }else{
                    $rate = array(
                        'id2' => $this->id,
                        'label' => "Presio de Envio",
                        'cost' => $precioDeEnvio );
                    $this->add_rate($rate);
                }
                
            }   
        }
    }
    add_action('woocommerce_shipping_init', 'cloudways_shipping_method');
    function add_cloudways_shipping_method($methods){

        $methods['wc_cloudways'] = 'wc_cloudways_Shipping_Method';
        return $methods;
    }
    add_filter('woocommerce_shipping_methods', 'add_cloudways_shipping_method');
}